/*
 * YamahaPAS Controller
 *
 * Created: 27/08/2022 9:15:39 PM
 * Author : Harshana
 * Command : avrdude.exe -c usbasp -p t13 -e -U "flash:w:D:\Creations\AVRC\YamahaPAS\Debug\YamahaPAS.hex:a"
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#define PIN_PWM	PB3 // pin 15
#define PIN_ECO PD0 // pin 2
#define PIN_LED1 PD1 // pin 3
#define PIN_LED2 PD2 // pin 4
#define PIN_LED3 PD3 // pin 5

#define PIN_TORQ PC0 // pin 23 (analog in)
#define PIN_BASE PC1 // pin 24 (analog in)
#define PIN_MFACTOR PC2 // pin 25 (analog in)
#define PIN_BLVL PC3 // pin 26 (analog in)
#define PIN_BTEMP PD7 // pin 13 (comparator in)

#define REG_ADMUX (1 << REFS0) | (1 << ADLAR)		// AVCC with external capacitor at AREF pin, Left Adjust Result
#define REG_ADCSRA (1 << ADEN) | (1 << ADSC) | (1 << ADPS1)		// 1100 0010 = ADC enable, start converting, pre-scalar 4 (256kHz @ 1MHz clock)

#define MULTI_DEVISOR 50	// multiply by 255/50 times
#define TIMEOUT_SEC 180		// timeout in 180 seconds

unsigned char ucMinTorq = 255; // Minimum Torque reading from torque sensor
unsigned char ucMFactor = 0; // Torque multiplication factor, reading from POT
unsigned char ucBasePWM = 0; // Base PWM value, reading from POT
unsigned char ucPWM = 0;  // final calculated PWM value;

unsigned char ucLEDs = 0;  // Battery level reading from POT
unsigned char ucError = 0;		// Error number

char cEco = 1;			// 1=Eco off, 2=Eco on
char cTicks = 0;		// timer0 overflow ticks (200ms each)
int iTOSec = 0;			// TimeOut seconds 


/* 
Errors 
Battery too low	- LED1 blinking
Battery temperature too high - LED2 blinking
*/

void ReadBasePWM();
void ReadMulFactor();
void ReadEcoMode();
void ReadTorq();
void ReadBatLevel();
void ReadBatTemp();
void ErrorFound();
void SleepForever();

// ============================================================================
int main(void)
{
	// ------------------------------ initialize digital output pins
	DDRB |= (1<<PIN_PWM);		// Output enable for PWM output
	DDRD |= (1<<PIN_LED1);		// Output enable for LED1 output
	DDRD |= (1<<PIN_LED2);		// Output enable for LED2 output
	DDRD |= (1<<PIN_LED3);		// Output enable for LED3 output

	// ------------------------------ initialize PWM timer
	TCCR2 = (1<<WGM21) | (1<<WGM20) | (1<<COM21) | (1<<CS22) | (1<<CS20);	// Fast PWM, Clear OC2 on Compare Match, set OC2 at BOTTOM (non-inverting mode), clk/128 (32Hz PWM freq=1Mhz/128 (divider)/256 (8bit counter))

	// ------------------------------ initialize Analog Comparator (battery temperature)
	ACSR = 0;		// all flags off. - ACSR.ACO will be set when +ve input is high
	
	// ------------------------------ initialize Timer0 for timing
	TCCR0 = (1<<CS02);   // clk/256 = 200ms overflow timer
	TIMSK = (1<<TOIE0);  // overflow interrupt for timer0
	
	// ------------------------------ watchdog timer
	wdt_enable(WDTO_1S);

	sei();

	// read these settings before going into run loop
	ReadBasePWM();
	ReadMulFactor();
	
	char cPrev = 0;
	
	while (1)
	{
		sleep_mode();
		
		if (iTOSec > TIMEOUT_SEC)
			SleepForever();

		if ((ucError == 0) && (cTicks != cPrev))
		{
			cPrev = cTicks;

			ReadEcoMode();			
			ReadTorq();
			OCR2 = ucPWM;	// set OutputCompareRegister for PWM output
			
			ReadBatLevel();
			PORTD |= ucLEDs;	// set LEDs based on battery level
			
			ReadBatTemp();
		}
	}
}

// ============================================================================
ISR(TIMER0_OVF_vect)
{
	wdt_reset();
	
	++cTicks;
	
	if (cTicks > 5) // look at this every 1 second
	{
		cTicks = 0;		
		
		if (iTOSec <= TIMEOUT_SEC)
			++iTOSec;
	}
	
	if (ucError > 0)
	{
		if ((PORTD &= ucLEDs) > 0)
			PORTD &= ~ucLEDs;
		else
			PORTD |= ucLEDs;
	}
}

// ===================================================================== Read Base PWM
void ReadBasePWM()
{
	ADMUX = REG_ADMUX | (1 << MUX0);		// AREF pin, left adj, use ADC1 input (PIN_BASE)
	ADCSRA = REG_ADCSRA;
	while (ADCSRA & _BV(ADSC));	// still converting

	ucBasePWM = ADCH;	
	
	return;
}

// ===================================================================== Read Multiplication Factor
void ReadMulFactor()
{
	ADMUX = REG_ADMUX | (1 << MUX1) | (1 << MUX0);		// AREF pin, left adj, use ADC3 input (PIN_MFACTOR)
	ADCSRA = REG_ADCSRA;
	while (ADCSRA & _BV(ADSC));	// still converting

	int val = ADCH + MULTI_DEVISOR;
	
	if (val > 255)
		ucMFactor = 255;
	else
		ucMFactor = val;
	
	return;
}

// ===================================================================== Read Eco Mode
void ReadEcoMode()
{
	cEco = 1; // eco off
	
	if (PORTD & _BV(PIN_ECO))  // if PIN set
		cEco = 2;				// eco mode on
				
	return;
}

// ===================================================================== ReadTorq
void ReadTorq()
{
	ADMUX = REG_ADMUX;		// AREF pin, left adj, use ADC0 input (PIN_TORQ)
	ADCSRA = REG_ADCSRA;
	while (ADCSRA & _BV(ADSC));	// still converting
	
	if (ucMinTorq > ADCH)  // correct for the minimum value
		ucMinTorq = ADCH;

	unsigned int val = ADCH - ucMinTorq;
	
	if (val < 10)
	{
		ucPWM = 0; // ignore small torque values;
		return;
	}
	
	iTOSec = 0;	   // reset timout timer
	
	val = val * ucMFactor / MULTI_DEVISOR + ucBasePWM / cEco;     // torq multiplication up to 5 times
	
	if (val > 255)
	{
		ucPWM = 255;  // full power
		return;
	}
	
	ucPWM = val;
	return;
}

// ===================================================================== Read BatteryLevel
void ReadBatLevel()
{
	ADMUX = REG_ADMUX | (1 << MUX1);		// AREF pin, left adj, use ADC2 input (PIN_BLVL)
	ADCSRA = REG_ADCSRA;
	while (ADCSRA & _BV(ADSC));	// still converting
	
	if (ADCH > 192)
		ucLEDs = (1<<PIN_LED1) | (1<<PIN_LED2) | (1<<PIN_LED3);
	else if (ADCH > 128)
		ucLEDs = (1<<PIN_LED1) | (1<<PIN_LED2);
	else if (ADCH > 64)
		ucLEDs = (1<<PIN_LED1);
	else
	{
		ucError = (1<<PIN_LED1);	// blink LED1 for battery too low
		ErrorFound();
	}
	
	return;
}

// ===================================================================== Read Battery Temperature
void ReadBatTemp()
{
	if ((ACSR & (1<<ACO)) > 0)
	{
		ucError = (1<<PIN_LED2);   // blink LED2 for high battery temp.
		ErrorFound();
	}
	
	return;
}

// ===================================================================== Error found
void ErrorFound()
{
	ucPWM = 0;	
	OCR2 = 0;	// stop motor
	
	ucLEDs = ucError;
	
	while(1)
		sleep_mode();		// never come out of this
}

// ===================================================================== Sleep forever
void SleepForever()
{
	PORTD &= ~((1<<PIN_LED1) | (1<<PIN_LED2) | (1<<PIN_LED3)); // switch off all LEDs
	wdt_disable();
	TIMSK = (1<<TOIE0);
		
	while(1)
		sleep_mode();		// never come out of this
}

